package ist.challenge.hanafi.constant;

public enum ResponseNum {
    SUCCESS(201, "sukses"),
    USEREXISTS(409, "username sudah terpakai"),
    WRONGPASS(401, "password salah"),
    NOTFOUND(404, "user tidak ditemukan"),
    EMPTY(400, "username dan atau password kosong"),
    LOGIN(200, "sukses login"),
    REPEATPASS(400,"password tidak boleh sama dengan sebelumnya")
    ;


    final Integer code;
    final String message;

    ResponseNum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
