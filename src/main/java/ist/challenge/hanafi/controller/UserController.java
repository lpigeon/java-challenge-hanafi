package ist.challenge.hanafi.controller;

import ist.challenge.hanafi.constant.ResponseNum;
import ist.challenge.hanafi.model.reqres.UserRequest;
import ist.challenge.hanafi.model.reqres.UserResponse;
import ist.challenge.hanafi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/registrasi")
    public ResponseEntity registrasi(@RequestBody UserRequest userRequest) {

        ResponseNum response = userService.register(userRequest);
        return ResponseEntity.status(response.getCode()).body(response.getMessage());
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UserRequest userRequest) {

        ResponseNum response = userService.login(userRequest);
        return ResponseEntity.status(response.getCode()).body(response.getMessage());
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserResponse>> getAll() {
        List<UserResponse> users = userService.getAllUsers();
        return ResponseEntity.status(HttpStatus.OK).body(users);
    }

    @PutMapping("/change/{id}")
    public ResponseEntity change(@PathVariable("id") Long id,
                          @RequestBody UserRequest userRequest){
        ResponseNum response = userService.changeUserPass(id, userRequest);
        return ResponseEntity.status(response.getCode()).body(response.getMessage());
    }


}
