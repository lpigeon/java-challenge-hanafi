package ist.challenge.hanafi.model;

import ist.challenge.hanafi.model.reqres.UserRequest;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "userr")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username", length = 25, unique = true)
    private String username;
    @Column(name = "password", length = 25)
    private String password;

    public User(UserRequest userRequest) {
        this.username=userRequest.getUsername();
        this.password=userRequest.getPassword();
    }

    public User(){}
}
