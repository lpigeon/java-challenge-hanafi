package ist.challenge.hanafi.model.reqres;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserRequest {
    private String username;
    private String password;
}
