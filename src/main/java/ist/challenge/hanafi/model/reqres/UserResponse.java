package ist.challenge.hanafi.model.reqres;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ist.challenge.hanafi.model.User;

public class UserResponse {

    @JsonSerialize
    private final Long id;
    @JsonSerialize
    private final String username;

    public UserResponse(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
    }
}
