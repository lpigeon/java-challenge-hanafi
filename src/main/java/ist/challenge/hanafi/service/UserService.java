package ist.challenge.hanafi.service;

import ist.challenge.hanafi.constant.ResponseNum;
import ist.challenge.hanafi.model.reqres.UserRequest;
import ist.challenge.hanafi.model.reqres.UserResponse;

import java.util.List;

public interface UserService {

    ResponseNum register(UserRequest userRequest);
    ResponseNum login(UserRequest userRequest);

    ResponseNum changeUserPass(Long id, UserRequest userRequest);

    List<UserResponse> getAllUsers();


}
