package ist.challenge.hanafi.service.impl;

import ist.challenge.hanafi.constant.ResponseNum;
import ist.challenge.hanafi.model.User;
import ist.challenge.hanafi.model.reqres.UserRequest;
import ist.challenge.hanafi.model.reqres.UserResponse;
import ist.challenge.hanafi.repository.UserRepository;
import ist.challenge.hanafi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;


    @Override
    public ResponseNum register(UserRequest userRequest) {
        if (!valid(userRequest))
            return ResponseNum.EMPTY;

        if (!userRepository.findByUsername(userRequest.getUsername()).isEmpty())
            return ResponseNum.USEREXISTS;

        userRepository.save(new User(userRequest));
        return ResponseNum.SUCCESS;
    }

    @Override
    public ResponseNum login(UserRequest userRequest) {
        if (!valid(userRequest)) {
            return ResponseNum.EMPTY;
        }
        Optional<User> user = userRepository.findByUsername(userRequest.getUsername());
        if (user.isEmpty())
            return ResponseNum.NOTFOUND;

        if(!user.get().getPassword().equals(userRequest.getPassword()))
            return ResponseNum.WRONGPASS;

        return ResponseNum.LOGIN;
    }

    @Override
    public ResponseNum changeUserPass(Long id, UserRequest userRequest) {

        Optional<User> oldUser = userRepository.findById(id);
        if(oldUser.isEmpty()) {
            return ResponseNum.NOTFOUND;
        }
        User changeUser = oldUser.get();
        if (changeUser.getPassword().equals(userRequest.getPassword())) {
            return ResponseNum.REPEATPASS;
        }
        changeUser.setUsername(userRequest.getUsername());
        changeUser.setPassword(userRequest.getPassword());

        try {
            userRepository.save(changeUser);
        } catch (Exception e) {
            return ResponseNum.USEREXISTS;
        }
        return ResponseNum.SUCCESS;
    }

    @Override
    public List<UserResponse> getAllUsers() {
        List<User> users =  userRepository.findAll();
        return users.stream().map(UserResponse::new).collect(Collectors.toList());
    }

    boolean valid(UserRequest userRequest) {
        return null != userRequest.getUsername() && !"".equals(userRequest.getUsername())
                && null != userRequest.getPassword() && !"".equals(userRequest.getPassword());
    }
}
